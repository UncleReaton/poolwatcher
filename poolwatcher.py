"""
		PoolWatcher by Hugo Mechiche
		16 December 2020

		The purpose of this program is 
		to alert by email when a new 
		Piscine date is available.
"""

# import libraries
import requests
from lxml import html
import smtplib, ssl
from email.mime.text import MIMEText

session_requests = requests.session()

login_url = "https://admissions.42.fr/users/sign_in"
result = session_requests.get(login_url)

#Getting the authenticity token
tree = html.fromstring(result.text)
authenticity_token = list(set(tree.xpath("//input[@name='authenticity_token']/@value")))[0] 

payload = {
	"user[email]": "mymail@mail.com",
	"user[password]": "mysitepassword",
	"authenticity_token" : authenticity_token
}

#Log into the site
result = session_requests.post(
	login_url, 
	data = payload, 
	headers = dict(referer=login_url)
)

#point to the right page
url = 'https://admissions.42.fr/users/myname_at_mail-com/id_checks_users'
result = session_requests.get(
	url, 
	headers = dict(referer = url)
)

#indicate what information we want to get
tree = html.fromstring(result.content)
piscine = tree.xpath("//div[@class='container']/ul[@class='list-group']/li[@class='list-group-item']/h3[@class='font-weight-bold surline']/text()")

#Piscines that are "avalaible" right now
piscinesActuelles = ['\nFÃƒÂ©vrier\n2021\n', '\nMars\n2021\n']

#if there is more information than in PiscinesActuelles then it'll send an email to me
if piscine != piscinesActuelles:
	message = MIMEText('Une nouvelle piscine est disponible sur https://admissions.42.fr/users/sign_in')
	message['Subject'] = '42 PISCINE DISPO'

	message['From'] = 'mymail@mail.com'
	message['To'] = 'mymail@mail.com'

	server = smtplib.SMTP('smtp.gmail.com:587')
	server.starttls()
	server.login('mymail@mail.com','mymailpassword')
	server.send_message(message)
	server.quit()
